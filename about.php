<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact | EscañoRealty</title>

    
    <?php include 'plugins-top.php';?>

</head>

<body ng-controller="mainCtrl">

    <!-- Navigator -->
    <?php include 'app/layouts/navigation.php';?>
    <!-- Navigator -->
    <!-- Slider -->
    <?php include 'app/layouts/slider.php';?>  
    <!-- Slider -->     

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span ng-bind="about.title | decodeUtf8"></span>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            
            <!-- Contact Details Column -->
            <div class="col-md-10">
                
                <span ng-bind-html="about.content | decodeUtf8"></span> 

                
            </div>
        </div>
        <!-- /.row -->        

        <hr>


    </div>
    <!-- /.container -->
    <!-- Footer -->     
    <?php include 'footer.php';?>
    <!-- /.container -->

    <?php include 'plugins-footer.php';?>
    <!-- Script to Activate the Carousel -->

</body>

</html>
