<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login|EscañoRealty</title>
  <link rel="icon" type="image/png" href="../public/img/logo/escanorealty_favicon.png">

  <!-- Bootstrap Core CSS -->
  <link href="../sbadmin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- MetisMenu CSS -->
  <link href="../sbadmin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
  <!-- textAngular CSS -->
    <link href="../../public/vendor/textAngular-master/dist/textAngular.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="../sbadmin/dist/css/sb-admin-2.css" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="../sbadmin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body ng-controller="LoginCtrl">

  <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
          <!-- <div class="panel-heading">
            <h3 class="panel-title">Please Sign In</h3>
          </div> -->

          <div class="panel-body">
            <form role="form">
              <fieldset>
                <div class="form-group">
                  <img src="../public/img/logo/escanorealty_favicon.png">
                </div>   
                <div class="form-group" ng-if="msg">
                  <div ng-bind="msg" class="alert alert-danger">                    
                  </div>
                </div>
                <div class="form-group">
                  <input class="form-control" ng-model="user.useremail" placeholder="E-mail or Username" name="email" type="text" autofocus>
                </div>
                <div class="form-group">
                  <input class="form-control" ng-model="user.password" placeholder="Password" name="password" type="password" value="">
                </div>
                <div class="checkbox">
                  <label>
                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                  </label>
                </div>
                <!-- Change this to a button or input when using this as a form -->
                <a href="" class="btn btn-lg btn-success btn-block" ng-click="login(user)">Login</a>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery -->
  <script src="../sbadmin/bower_components/jquery/dist/jquery.min.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="../sbadmin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- Metis Menu Plugin JavaScript -->
  <script src="../sbadmin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

  <!-- Custom Theme JavaScript -->
  <script src="../sbadmin/dist/js/sb-admin-2.js"></script>

  <!-- AngularJS -->
  <script src="../public/js/angular/angular.js"></script>
  <script src="../public/js/angular/be/controllers/app.js"></script>
  <script src="../public/js/angular/be/controllers/config.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
  <script src="//rawgit.com/angular-ui/angular-google-maps/2.1.5/dist/angular-google-maps.min.js"></script>
    

  <script src="../public/vendor/angular-storage-master/dist/angular-storage.min.js"></script>
  <!-- // <script src="../../public/vendor/angular-sanitize.min.js"></script> -->
  <script src="../public/vendor/ui-bootstrap-tpls.min.js"></script>
  <script src="../public/vendor/ng-file-upload.min.js"></script>

  <!-- slugifier -->
  <script src="../../public/vendor/angular-slugify.js"></script>

  <!-- textAngular -->
  <script src="../public/vendor/textAngular-master/dist/textAngular-rangy.min.js"></script>
  <script src="../public/vendor/textAngular-master/dist/textAngular-sanitize.min.js"></script>
  <script src="../public/vendor/textAngular-master/dist/textAngular.min.js"></script>

</body>

</html>
