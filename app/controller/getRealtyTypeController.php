<?php
include '../config/config.php';

  
  
  $query = "SELECT * FROM realty_type";
  $query .= " ORDER BY type ASC";

  $result = $conn->query($query);
  if($result){
    while ($row = $result->fetch_object()) { 
      $results_array[] = $row;
    } 
  }
  
  header('Content-Type: application/json');
  $conn->close();
  
  echo json_encode($results_array);
?>
