<?php
include '../config/config.php';
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
	$query = "SELECT * FROM real_estate 
	LEFT JOIN realty_type ON 
	real_estate.category = realty_type.id 
	WHERE real_estate.status=1";
	if($request->category){
		$query .= " AND real_estate.category LIKE '%".$request->category."%'";
	}
	if($request->address){
		$query .= " AND real_estate.address LIKE '%".$request->address."%'";
	}


	$query .= " ORDER BY real_estate.id DESC";

	$result = $conn->query($query);
	$results_array = false;  
	if($result){

		while ($row = $result->fetch_object()) {  
			$query2 = "SELECT * FROM real_state_images WHERE real_state_id='".$row->real_state_id."'";
			$query2 .= " ORDER BY id ASC LIMIT 1";
			$images = $conn->query($query2);
			if($images){
				while ($img_row = $images->fetch_object()) { 
					$img_array = $img_row->file_name;

				} 
			}
			$data[] = array(
				'real_state_id' => $row->real_state_id,
				'title' => $row->title,
				'description' => $row->description,
				'address' => $row->address,
				'orig_price' => $row->orig_price,
				'discounted_price' => $row->discounted_price,
				'category' => $row->type,
				'status' => $row->status,
				'date_created' => $row->date_created,
				'date_updated' => $row->date_updated,
				'image' => $img_array
				);  


			$results_array = $data;   
		} 
	}else{
		$results_array = false;  
	}
  	// Set session variables


	$conn->close();  
}
echo json_encode($results_array);

?>
