<?php
include '../config/config.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
  
  
  $msgID = uniqid();
  
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "INSERT INTO messages SET";
  $sql .= " 
  msgID = '".$msgID."', 
  fullname = '".$request->fullname."',
  email = '".$request->email."',
  contactNumber = '".$request->contactNumber."',
  msg = '".$request->msg."',
  status = 0,
  date_created = '".date('Y-m-d H:i:s')."',
  date_updated = '".date('Y-m-d H:i:s')."'
  ";

  if ($conn->query($sql) === TRUE) {
    $data = array(
      'result' => 'success',
      'msgID' => $msgID
      );
  } else {
    $data = array(
      'result' => 'error',
      'error_msg' => $conn->error
      );
  }  
  // $data = $row; 
  $conn->close();
  // Set session variables
  
  echo json_encode($data);
  
}


?>
