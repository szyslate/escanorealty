<script type="text/ng-template" id="view-adds.html">
	<div ng-include="'/public/tpls/fe/view-adds.html'"></div>
</script>
<!-- Adds CSS -->
<link href="public/css/adds-slider.css" rel="stylesheet">

<div class="adds-wrap" ng-controller="addsCtrl">
	<div class="row">
		<div class="col-md-2 our-partners-bg"> 
			<div class="our-partners">PARTNERS</div>
		</div>
		<div class="col-md-10 adds-bg"> 
			<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 809px; height: 120px; overflow: hidden; visibility: hidden;">
				<!-- Loading Screen -->
				<div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
					<div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
					<div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
				</div>
				<div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 809px; height: 120px; overflow: hidden;">
					<div ng-repeat="addsList in addsList" style="display: none;text-align:center;">
						<div class="adds-wrap-img">
							<img ng-click="viewAdds(addsList)" class="adds-img" src="public/uploads/adds/{[{addsList.image}]}" height=50>
						</div>
						
						<!-- <div class="adds-slide" style="background-image: url('public/uploads/adds/{[{addsList.image}]}');"> -->
							
						<!-- </div> -->
					</div>
					<!-- <div style="display: none;text-align:center;">
						Lamudi.ph
					</div>
					<div style="display: none;text-align:center;">
						Saitama
					</div>
					<div style="display: none;text-align:center;">
						Hokage
					</div>
					<div style="display: none;text-align:center;">
						Kasla Awan
					</div>
					<div style="display: none;text-align:center;">
						Gunting
					</div>
					<div style="display: none;text-align:center;">
						Mouse
					</div>
					<div style="display: none;text-align:center;">
						Keyboard
					</div>
					<div style="display: none;text-align:center;">
						Tinapay
					</div>
					<div style="display: none;text-align:center;">
						Touch Screen
					</div>
					<div style="display: none;text-align:center;">
						Relo
					</div> -->

				</div>
			</div>
		</div>
	</div>
</div>
<script src="public/js/jssor.slider.mini.js"></script>
<!-- <script src="public/js/adds-slider.js"></script>