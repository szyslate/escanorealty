<script type="text/ng-template" id="view-realty.html">
  <div ng-include="'/public/tpls/fe/view-realty.html'"></div>
</script>
<div class="row" ng-controller="searchCtrl">
	<div class="col-lg-12">
		<h4 class="page-header">Search Result</h4>
	</div>
	<div ng-show="list != 'false'">
		<div class="col-md-4 col-sm-6" ng-repeat="list in list | limitTo: paginationLimit()" ng-click="view(list.real_state_id)">
			<div class="realty-list" style="background-image: url('public/uploads/{[{list.real_state_id}]}/{[{list.image}]}');">
				<label class="realty-details">
					<span ng-bind="list.title"></span>
				</label>           
			</div>  
		</div>
		<div class="col-sm-12 center">
			<button class="btn btn-outline btn-primary btn-sm" ng-show="hasMoreItemsToShow()" ng-click="showMoreItems()">
				Show more
			</button>   
		</div> 
	</div>
	<div ng-show="list == 'false'">
		<div class="col-sm-12 center">
			 <p>No record found.</p>
		</div> 
	</div>
	          
</div>