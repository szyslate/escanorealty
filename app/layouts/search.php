<?php include 'paths.php';?>
<div class="search-bar" ng-controller="searchCtrl">            
	<div class="row">            
		<div class="col-sm-12">		
			<label>Search Property: </label>				
		</div>           
		<div class="col-sm-5">
			<!-- <label>Property Type: </label> -->
			<select ng-model="search.type" class="form-control">
				<option value="">Property Type</option>
				<option ng-repeat="realtyType in realtyType" value="{[{realtyType.id}]}" ng-bind="realtyType.type"></option>
			</select>
		</div>          
		<div class="col-sm-5">
			<!-- <label>Location: </label> -->
			<!-- <select class="form-control">
				<option value="">Location</option>
			</select> -->
			<input type="text" ng-model="search.key" placeholder="Region, Provice, City, Town or etc...." class="form-control" id="name" >
		</div>         
		<!-- <div class="col-sm-3"> -->
			<!-- <label>Price Range: </label> -->
			<!-- <select class="form-control"> -->
				<!-- <option value="">Price Range</option> -->
			<!-- </select> -->
		<!-- </div>          -->
		<div class="col-sm-2">
			<button ng-click="goSearch(search)" class="btn btn-default btn-block" type="button"><i class="fa fa-search"></i></button>
		</div>
	</div>
</div>