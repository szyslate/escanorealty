<?php include 'paths.php';?>
<!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item" ng-repeat="imgList in imgList" ng-class='{active:$first}'>
                <div class="fill slider-img" style='background-image:url("/public/uploads/slider/{[{imgList.imgName}]}");'></div>
                <div class="carousel-caption">
                    <h2></h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" data-target="#myCarousel" data-slide="prev" ng-non-bindable>
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" data-target="#myCarousel" data-slide="next" ng-non-bindable>
            <span class="icon-next"></span>
        </a>
    </header>