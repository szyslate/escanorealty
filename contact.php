<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact | EscañoRealty</title>

    
    <?php include 'plugins-top.php';?>

</head>

<body ng-controller="mainCtrl">

    <!-- Navigator -->
    <?php include 'app/layouts/navigation.php';?>
    <!-- Navigator -->
    <!-- Slider -->
    <?php include 'app/layouts/slider.php';?>  
    <!-- Slider -->     

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Contact
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Content Row -->
        <div class="row">
            <!-- Map Column -->
            <div class="col-md-8">
                <div ng-controller="mapCtrl">
                    <div id="googleMap" style="width:100%;height:400px;"></div>
                </div>                
            </div>
            <!-- Contact Details Column -->
            <div class="col-md-4">
                <h3>Contact Details</h3>
                
                <span ng-bind-html="contact.content | decodeUtf8"></span> 

                <ul class="list-unstyled list-inline list-social-icons">
                    <li>
                        <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-google-plus-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->

        <!-- Contact Form -->
        <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
        <div ng-controller="contactCtrl">
            <div class="row">
                <div class="col-md-8">
                    <h3>Send us a Message</h3>
                    <form ng-submit="submit(msgData)" name="sentMessage" id="contactForm" novalidate>
                        <div class="control-group form-group">
                        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Full Name: <i class="req">*</i></label>
                                <input ng-model="msgData.fullname" type="text" class="form-control" id="name" required>
                                <p class="help-block"></p>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Phone Number: <i class="req">*</i></label>
                                <input ng-model="msgData.contactNumber" type="tel" class="form-control" id="phone" numbers-only required>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Email Address: <i class="req">*</i></label>
                                <input ng-model="msgData.email" ng-blur="chkEmail(msgData.email)" type="email" class="form-control" id="email" required>
                                <span ng-if="validEmail" class="period req">Invalid Email</span>
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <div class="controls">
                                <label>Message: <i class="req">*</i></label>
                                <textarea ng-model="msgData.msg" rows="10" cols="100" class="form-control" id="message" required maxlength="999" style="resize:none"></textarea>
                            </div>
                        </div>
                        <div id="success"></div>
                        <button type="submit" class="btn btn-primary">Send Message</button>
                    </form>
                </div>

            </div>
        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->
    <!-- Footer -->     
    <?php include 'footer.php';?>
    <!-- /.container -->

    <?php include 'plugins-footer.php';?>
    <!-- Script to Activate the Carousel -->

</body>

</html>
