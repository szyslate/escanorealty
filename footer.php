<div class="footer bgcolor"> 
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-4"> 
					<h5 class="page-header">
					<span ng-bind="about.title | decodeUtf8"></span>
					</h5>
					<span ng-bind-html="about.content | decodeUtf8"></span>
				</div>
				<div class="col-sm-4"> 
					<h5 class="page-header">
						<span>Sevices</span>
					</h5>
					<span ng-bind-html="service.content | decodeUtf8"></span>               
				</div>
				<div class="col-sm-4"> 
					<ul style="margin-top:30px;margin-bottom:25px;" class="list-unstyled list-inline list-social-icons">
						<li ng-if="social.facebook">
							<a href="{[{social.facebook}]}" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a>
						</li>
						<li ng-if="social.twitter">
							<a href="{[{social.twitter}]}" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a>
						</li>
						<li ng-if="social.linkedin">
							<a href="{[{social.linkedin}]}" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a>
						</li>
						<li ng-if="social.instagram">
							<a href="{[{social.instagram}]}"><i class="fa fa-instagram fa-2x"></i></a>
						</li>
						<li ng-if="social.google">
							<a href="{[{social.google}]}" target="_blank"><i class="fa fa-google-plus-square fa-2x"></i></a>
						</li>
						<li ng-if="social.youtube">
							<a href="{[{social.youtube}]}" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a>
						</li>
					</ul>
					<span ng-bind-html="contact.content | decodeUtf8"></span>               
				</div>
			</div>
		</div>
	</footer>
</div>