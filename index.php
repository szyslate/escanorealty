<?php include 'app/config/config.php';?>
<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EscañoRealty</title>
    <?php include 'plugins-top.php';?>

</head>

<body ng-controller="mainCtrl">
    <!-- Navigator -->
    <?php include 'app/layouts/navigation.php';?>
    <!-- Navigator -->
    <!-- Slider -->
    <?php include 'app/layouts/slider.php';?>  
    <!-- Slider -->    

    <!-- Page Content -->
    <div class="container">
        
        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <span ng-bind="datamain.title | decodeUtf8"></span>
                </h1>
            </div>
            <div class="col-md-12">
                <p ng-bind-html="datamain.content | decodeUtf8"></p>                
            </div>
        </div>
        <!-- /.row -->

        <!-- Search -->
        <?php include 'app/layouts/search.php';?>  
        <!-- Search -->

        <!-- Realty List Section -->
        <?php include 'app/layouts/realty-list.php';?>  
        <!-- /.row -->
        <hr>
        


    </div>
        <!-- Adds Section -->
    <?php include 'app/layouts/adds.php';?>     
    <!-- Call to Map Section -->
    <div ng-controller="mapCtrl">        
        <div id="googleMap" style="width:100%;height:380px;"></div>
    </div>
    <!-- Footer -->     
    <?php include 'footer.php';?>
    <!-- /.container -->

    <?php include 'plugins-footer.php';?>
    <!-- Script to Activate the Carousel -->
    

</body>

</html>
