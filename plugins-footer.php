 <!-- jQuery -->
<script src="public/js/jquery.js"></script>



 <!-- AngularJS -->
<script src="public/js/angular/angular.js"></script>
<script src="public/js/angular/fe/controllers/app.js"></script>    
<script src="public/js/angular/fe/controllers/config.js"></script>
<script src="public/js/angular/directives.js"></script>
<script src="public/js/angular/filters.js"></script>

 <!-- Controllers -->
<script src="public/js/angular/fe/controllers/mainCtrl.js"></script>
<script src="public/js/angular/fe/controllers/realtyListCtrl.js"></script>
<script src="public/js/angular/fe/controllers/mapCtrl.js"></script>
<script src="public/js/angular/fe/controllers/searchCtrl.js"></script>
<script src="public/js/angular/fe/controllers/addsCtrl.js"></script>
<script src="public/js/angular/fe/controllers/socialMediaCtrl.js"></script>
<script src="public/js/angular/fe/controllers/contactCtrl.js"></script>

<script
src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCbdYfbrRhOiLgmIec6d1I7rHjZ6eoY7XM">
</script>
<script src="public/vendor/angular-storage-master/dist/angular-storage.min.js"></script>    
<script src="public/vendor/ui-bootstrap-tpls.min.js"></script>
<script src="public/vendor/textAngular-master/dist/textAngular-sanitize.min.js"></script>
 
<!-- Bootstrap Core JavaScript -->
<script src="public/js/bootstrap.min.js"></script>

<script>
	$('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>

