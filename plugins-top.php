<link rel="icon" type="image/png" href="public/img/logo/escanorealty_favicon.png">

<!-- Bootstrap Core CSS -->
<link href="public/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="public/css/sb-admin-2.css" rel="stylesheet">
<link href="public/css/modern-business.css" rel="stylesheet">



<!-- Custom Fonts -->
<link href="public/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="public/css/custom.css">
<link rel="stylesheet" href="public/css/bgcolors.css">
<link rel="stylesheet" href="public/css/carousel.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
