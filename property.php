<?php include 'app/config/config.php';?>
<!DOCTYPE html>
<html lang="en" ng-app="myModule">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EscañoRealty</title>
    <?php include 'plugins-top.php';?>

</head>

<body ng-controller="mainCtrl">
    <!-- Navigator -->
    <?php include 'app/layouts/navigation.php';?>
    <!-- Navigator -->
    <!-- Slider -->
    <?php include 'app/layouts/slider.php';?>  
    <!-- Slider -->    

    <!-- Page Content -->
    <div class="container">
        
        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Real Estates
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Search -->
        <?php include 'app/layouts/search.php';?>  
        <!-- Search -->

        <!-- Realty List Section -->
        <?php include 'app/layouts/realty-search.php';?>        
        


        <hr>


    </div>
    <!-- Footer -->     
    <?php include 'footer.php';?>
    <!-- /.container -->

    <?php include 'plugins-footer.php';?>
    <!-- Script to Activate the Carousel -->
    

</body>

</html>
