'use strict';
// uiGmapGoogleMapApi,
myApp.controller('addsPageCtrl', function($scope,$location,$http,$window,$sce,$interval,$timeout,store,$modal,Upload,Config,Slug) {

  console.log(Config.baseURL);

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var listAdds = function () {
    var requestGET = $http({
      method: "get",
      url: "controller/listAddsController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {        
      console.log(data);
      if(data){
        $scope.list = data;
      }      
    });
  }
  listAdds();

  $scope.createAdds = function (){
    var modalInstance = $modal.open({
      templateUrl: 'add-adds.html',
      controller: createAddsCtrl,
      windowClass: 'app-modal-create',
      backdrop: 'static'
    });
  }
  var createAddsCtrl = function ($scope, $modalInstance) {
    $scope.data = {};
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
    $scope.browseImage = function (files) {      
        $scope.imgFiles = files;        
    }
    $scope.saveAdds = function (datus,file,status) { 
      $scope.closeAlert();    
      if(datus.title && datus.description && file){
        datus['status'] = status;
        var request = $http({
          method: "post",
          url: "controller/addAddsController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {        
          console.log(data);
          if (file) {
              Upload.upload({
                url: '../../public/uploads/upload.php',
                data: {
                  file: file, 
                  'adds_id': data.adds_id,
                  'folder': "adds",
                }
              }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
              }, function (resp) {
                console.log('Error status: ' + resp.status);
              }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
          }
          $scope.data = {};
          $scope.imgFiles = {};
          $scope.alerts.push({type: 'success', msg: 'Adds saved.'});
          listAdds();
        }); 
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please check all the fields with (*) and fill up.'});
      }       
    } 
  }

  $scope.editAdds = function (item){
    var modalInstance = $modal.open({
      templateUrl: 'edit-adds.html',
      controller: editAddsCtrl,
      windowClass: 'app-modal-create',
      backdrop: 'static',
      resolve: {
        item: function () {
          return item;
        }
      }
    });
  }
  var editAddsCtrl = function ($scope, $modalInstance, item) {
    console.log(item);
    $scope.datas = {};
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
    $scope.browseImage = function (files) {      
        $scope.imgFiles = files;        
    }
    $scope.datas = item;
    $scope.saveChange = function (datus,file,status) { 
      $scope.closeAlert();    
      if(datus.title && datus.description){
        datus['status'] = status;
        var request = $http({
          method: "post",
          url: "controller/saveChangeAddsController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {        
          console.log(data);
          if (file) {
            item['type'] = 'adds';
            item['imgName'] = data.image;
            var dltrequest = $http({
              method: "post",
              url: "../../public/uploads/delete.php",
              data: item,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });
            dltrequest.success(function (data) {console.log(data);});

            Upload.upload({
              url: '../../public/uploads/upload.php',
              data: {
                file: file, 
                'adds_id': data.adds_id,
                'folder': "adds",
              }
            }).then(function (resp) {
              console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          }
          listAdds();          
          $scope.alerts.push({type: 'success', msg: 'Changes saved.'});
        }); 
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please check all the fields with (*) and fill up.'});
      }       
    } 
  }

  $scope.deleteAdds = function (item){
    var modalInstance = $modal.open({
      templateUrl: 'delete-adds.html',
      controller: deleteAddsCtrl,
      resolve: {
        item: function () {
          return item;
        }
      }
    });        
    modalInstance.result.then(
      function (data) {
        $scope.closeAlert();
        listAdds();
        $scope.alerts.push({type: data.type, msg: data.result});
      }
      );
  }
  var deleteAddsCtrl = function ($scope, $modalInstance, item) {    
    $scope.item = item;
    console.log(item);
    $scope.alerts = [];
    $scope.ok = function (item){
      if(item){
        item['type'] = 'adds';
        var request = $http({
          method: "post",
          url: "../../public/uploads/delete.php",
          data: item,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        request.success(function (data) { 
          $modalInstance.close(data);        
        });
      }
    }
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
  };



});