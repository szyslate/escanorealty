'use strict';

var myApp = angular.module('myModule', [
  'angular-storage',
  'ui.bootstrap',  
  'uiGmapgoogle-maps',
  'ngSanitize',
  'ngFileUpload',
  'textAngular',
  'slugifier'
  ]);

myApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});

myApp.config(function(uiGmapGoogleMapApiProvider) {
 uiGmapGoogleMapApiProvider.configure({
  key: 'AIzaSyCbdYfbrRhOiLgmIec6d1I7rHjZ6eoY7XM',
  libraries: 'weather,geometry,visualization'
 });
});


myApp.controller('mainCtrl', function($scope,$location,$window,store) {
  // $scope.baseURL = window.location.href;
  $scope.baseURL = $location.host();
  $scope.userAdmin = store.get('useradmin');
  console.log(store.get('useradmin'));
  if(!$scope.userAdmin){
    $window.location.href = '../../index.php';
  }   
  $scope.logout = function (item){
    store.remove('useradmin');
    $window.location.href = '../../admin/index.php';
  };
});

myApp.controller('LoginCtrl', function($scope,$location,$http,$window,store) {
  // $scope.baseURL = window.location.href;
  $scope.baseURL = $location.host();  
  $scope.msg = '';  

  $scope.login = function (user){
  	// $scope.display = "pindot";
  	var request = $http({
  		method: "post",
  		url: "loginController.php",
  		data: {
  			email: user.useremail,
  			pass: user.password
  		},
  		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
  	});
  	/* Check whether the HTTP Request is successful or not. */
  	request.success(function (data) {
  		if(data){
        store.set('useradmin', data);
  		}		
  		(data) ? $window.location.href = '../sbadmin/pages/index.php' : false;
  		
  	}); 	
  	
  }

});

myApp.factory('mapsFactory', function() {
    return {
        getMaps: function(data) {
            return data
        }
    };
});