'use strict';
// 
myApp.controller('contactPageCtrl', function(uiGmapGoogleMapApi,$scope,$location,$http,$window,$sce,$interval,$timeout,store,$modal,Upload,Config,Slug) {

  console.log(Config.baseURL);

  var getContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "contact"
      },
      url: "controller/getPageContentController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {   
      if(data){
        $scope.data = data;
      }      
    });
  }
  getContent();

  var makeLoc = function () {

  };
  var getMap = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "map"
      },
      url: "controller/getPageContentController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {   
      if(data){
        console.log(data);
        $scope.locmap = data;
        var y = data.content.replace(/[\s]/g, '');
        var x = y.split(",")
        $scope.latLong = {
          latitude: parseFloat(x[0]),
          longitude: parseFloat(x[1])
        };
      }      
    });
  }
  getMap();

  $scope.latLong = {
    latitude: 15.996711,
    longitude: 120.675311
  };

  $scope.pasteMap = function (mapUrl){
    var y = mapUrl.replace(/[\s]/g, '');
    var x = y.split(",")
    $scope.latLong = {
      latitude: parseFloat(x[0]),
      longitude: parseFloat(x[1])
    };
    console.log($scope.latLong);  
    
  }
  $scope.$watch('latLong', function() {   
    uiGmapGoogleMapApi.then(function(maps) {    
      $scope.map = {
        center: {
          latitude: $scope.latLong.latitude,
          longitude: $scope.latLong.longitude
        },
        zoom: 16
      };
      $scope.options = {
        scrollwheel: false
      };
      $scope.coordsUpdates = 0;
      $scope.dynamicMoveCtr = 0;
      $scope.marker = {
        id: 0,
        coords: {
          latitude: $scope.latLong.latitude,
          longitude: $scope.latLong.longitude
        },
        options: {
          draggable: false,
          labelContent: $scope.locmap.title,
          labelAnchor: "100 10",
          labelClass: "marker-labels"
        }
      };
    });
  });

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };  

  $scope.$watch('data', function() {
    if($scope.data){
      $scope.data.title = decodeURIComponent(escape($scope.data.title));
    }        
  });

  $scope.saveChange = function (datus){
      $scope.closeAlert();
      if(datus.title && datus.content){
        var request = $http({
          method: "post",
          url: "controller/saveChangeMainPageController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          console.log(data);
          $scope.alerts.push({type: data.type, msg: data.result});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please fill up the fields with (*).'});
      }  
    }




});