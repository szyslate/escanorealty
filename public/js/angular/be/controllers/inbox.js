'use strict';
// uiGmapGoogleMapApi,
myApp.controller('inboxCtrl', function($scope,$location,$http,$window,$sce,$interval,$timeout,store,$modal,Upload,Config,Slug) {

  console.log(Config.baseURL);

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var messagesData = function () {
    var requestGET = $http({
      method: "get",
      url: "controller/messagesController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {        
      console.log(data);
      if(data){
        $scope.msgData = data;
      }      
    });
  }
  messagesData();

  $scope.viewMsg = function (item){
    var modalInstance = $modal.open({
      templateUrl: 'view-msg.html',
      controller: viewMsgCtrl,
      backdrop: 'static',
      resolve: {
        item: function () {
          return item;
        }
      }
    });
  }
  var viewMsgCtrl = function ($scope, item, $modalInstance) {
    $scope.item = item;
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
  }

 

  $scope.deleteAdds = function (item){
    var modalInstance = $modal.open({
      templateUrl: 'delete-adds.html',
      controller: deleteAddsCtrl,
      resolve: {
        item: function () {
          return item;
        }
      }
    });        
    modalInstance.result.then(
      function (data) {
        $scope.closeAlert();
        listAdds();
        $scope.alerts.push({type: data.type, msg: data.result});
      }
      );
  }
  var deleteAddsCtrl = function ($scope, $modalInstance, item) {    
    $scope.item = item;
    console.log(item);
    $scope.alerts = [];
    $scope.ok = function (item){
      if(item){
        item['type'] = 'adds';
        var request = $http({
          method: "post",
          url: "../../public/uploads/delete.php",
          data: item,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        request.success(function (data) { 
          $modalInstance.close(data);        
        });
      }
    }
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
  };



});