'use strict';

myApp.controller('mainPageCtrl', function($scope,$location,$http,$window,store,$modal,Upload,Config,Slug) {

  console.log(Config.baseURL);

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var getContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "mainpage"
      },
      url: "controller/getPageContentController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {   
      if(data){
        $scope.data = data;
      }
      
    });
  }
  getContent();

  $scope.$watch('data', function() {
    if($scope.data){
      $scope.data.title = decodeURIComponent(escape($scope.data.title));
      $scope.data.content = decodeURIComponent(escape($scope.data.content));
    }        
  });

  $scope.saveChange = function (datus){
      $scope.closeAlert();
      if(datus.title&& datus.content){
        var request = $http({
          method: "post",
          url: "controller/saveChangeMainPageController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          console.log(data);
          $scope.alerts.push({type: data.type, msg: data.result});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please fill up the fields with (*).'});
      }  
    }




});