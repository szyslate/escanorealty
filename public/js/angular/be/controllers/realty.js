'use strict';

myApp.controller('addPropertyCtrl', function($scope,$location,$http,$window,$timeout,store,$modal,Upload,Config,Slug,mapsFactory) {

  console.log(Config.baseURL);

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var goToMap = function (xlat,ylong,title) {
    var styleArray = [
    {
      featureType: "all",
      stylers: [
      { saturation: -80 }
      ]
    },{
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
      { hue: "#00ffee" },
      { saturation: 50 }
      ]
    },{
      featureType: "poi.business",
      elementType: "labels",
      stylers: [
      { visibility: "off" }
      ]
    }
    ];
    var myCenter=new google.maps.LatLng(xlat, ylong);
    var mapProp = {
      center:myCenter,
      styles: styleArray,
      scrollwheel: false,
      zoom:16,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("googleMapProp"),mapProp);

    var marker=new google.maps.Marker({
      map: map,
      position:myCenter,
      title: title
    });
    marker.setMap(map);
  }

  var listRealty = function () {
    var requestGET = $http({
      method: "get",
      url: "controller/listRealtyController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {        
      console.log(data);
      if(data){
        $scope.list = data;
      }      
    });
  }
  listRealty();

  $scope.deleteRealty = function (item){
    var modalInstance = $modal.open({
      templateUrl: 'delete-realty.html',
      controller: deleteRealtyCtrl,
      resolve: {
        item: function () {
          return item;
        }
      }
    });        
    modalInstance.result.then(
      function (data) {
        $scope.closeAlert();
        $scope.alerts.push({type: data.type, msg: data.result});
      }
      );
  }
  var deleteRealtyCtrl = function ($scope, $modalInstance, item) {    
    $scope.item = item;
    console.log(item);
    $scope.alerts = [];
    $scope.ok = function (item){
      if(item){
        var request = $http({
          method: "post",
          url: "controller/deleteRealtyController.php",
          data: item,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        request.success(function (data) { 
          // $modalInstance.close('cancel');
          $modalInstance.close(data);         
          listRealty();
          // $scope.alerts.push({type: data.type, msg: data.result});          
        });
      }
    }
    $scope.close = function () {
      $modalInstance.close('cancel'); 
    };
  };

  $scope.addProp = function (){
    var modalInstance = $modal.open({
      templateUrl: 'add-realty.html',
      controller: addProperty,
      windowClass: 'app-modal-create',
      backdrop: 'static'
    });
  }
  var addProperty = function ($scope, $modalInstance) {
    $scope.data = {};
    
    var getCat = function () {
      var requestGET = $http({
        method: "get",
        url: "controller/getCategoryController.php",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {        
        console.log(data);
        if(data){
          $scope.catList = data;
        }      
      });
    }
    getCat(); 

    /* browse image */
    $scope.browseFiles = function (files) {      
        $scope.imgFiles = files;        
    }   
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.showMap = false;  
    $scope.pasteMap = function (data) { 
      var y = data.map.replace(/[\s]/g, '');
      var x = y.split(",");
      goToMap(parseFloat(x[0]),parseFloat(x[1]),data.title);
      $scope.showMap = true;      
    }
   
    $scope.save = function (datus,file,status){
      $scope.closeAlert();

      if(datus.title && datus.description && datus.address && datus.orig_price && datus.category && datus.map && file){
        datus['status'] = status;
        
        var request = $http({
          method: "post",
          url: "controller/addRealtyController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {        
          console.log(data);
          if (file && file.length) {
            for (var i = 0; i < file.length; i++) {              
              console.log(file[i]);
              Upload.upload({
                url: '../../public/uploads/upload.php',
                data: {
                  file: file[i], 
                  'real_state_id': data.real_state_id,
                  'folder': data.real_state_id,
                }
              }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
              }, function (resp) {
                console.log('Error status: ' + resp.status);
              }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
            }
          }
          $scope.data = {};
          $scope.imgFiles = {};
          listRealty();
          $scope.alerts.push({type: 'success', msg: 'Property saved.'});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please check all the tabs and fill up the fields with (*).'});
      }  

    }
    
    $scope.close = function () {
      $modalInstance.dismiss('cancel');
      listRealty();
    };
  }; 

  /*======================================================*/ 
  /*++++++++++++++++++++ edit section ++++++++++++++++++++*/ 
  /*======================================================*/ 

  $scope.editProp = function (itemID){
    var modalInstance = $modal.open({
      templateUrl: 'edit-realty.html',
      controller: editProperty,
      windowClass: 'app-modal-create',
      backdrop: 'static',
      resolve: {
        itemID: function () {
          return itemID;
        }
      }
    });
  }
  var editProperty = function ($scope, $modalInstance, itemID) {
    
    var getCat = function () {
      var requestGET = $http({
        method: "get",
        url: "controller/getCategoryController.php",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {        
        console.log(data);
        if(data){
          $scope.catList = data;
        }      
      });
    }
    getCat();

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.data = {};
    $scope.onEdit = false;
    $scope.edit = function () {$scope.onEdit = true;}
    $scope.cancelEdit = function () {
      $scope.onEdit = false;
      getRealty();
    };

    $scope.onEditDesc = false;
    $scope.editDesc = function () {$scope.onEditDesc = true;}
    $scope.cancelEditDesc = function () {
      $scope.onEditDesc = false;
      getRealty();
    };

    $scope.onEditMap = false;
    $scope.editMap = function () {$scope.onEditMap = true;}
    $scope.cancelEditMap = function () {
      $scope.onEditMap = false;
      getRealty();
    };

    $scope.showMap = true;  
    $scope.pasteMap = function (data) { 
      var y = data.map.replace(/[\s]/g, '');
      var x = y.split(",")
      goToMap(parseFloat(x[0]),parseFloat(x[1]),data.title);
      $scope.showMap = true;      
    };

    var getRealty = function () {
      var requestGET = $http({
        method: "post",
        url: "controller/getRealtyController.php",
        data: {
          real_state_id: itemID
        },
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {        
        
        if(data){
          var prop = data.data;
          $scope.data = prop;
          $scope.imgFiles = data.image;
          var y = prop.map.replace(/[\s]/g, '');
          var x = y.split(",");  
          $timeout(function() {
            goToMap(parseFloat(x[0]),parseFloat(x[1]),prop.title); 
          }, 2000);
        }
      });
    }
    getRealty();

    $scope.markAs = function (itemID,status){
      $scope.closeAlert();
      if(itemID){
        
        var request = $http({
          method: "post",
          url: "controller/saveStatusRealtyController.php",
          data: {
          real_state_id: itemID,
          status: status
        },
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          listRealty();
          getRealty();
          console.log(data);
          $scope.alerts.push({type: 'success', msg: data.result});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please fill up the fields with (*) on the Details Tab.'});
      }  
    }

    $scope.saveChangeDetails = function (datus){
      $scope.closeAlert();
      if(datus.title && datus.address && datus.orig_price && datus.category){
        
        var request = $http({
          method: "post",
          url: "controller/saveChangeDetailsRealtyController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          listRealty();
          $scope.onEdit = false;
          $scope.alerts.push({type: 'success', msg: 'Property updated.'});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Please fill up the fields with (*) on the Details Tab.'});
      }  
    }

    $scope.saveChangeDesc = function (datus){
      $scope.closeAlert();
      if(datus.description){
        
        var request = $http({
          method: "post",
          url: "controller/saveChangeDescRealtyController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          listRealty();
          $scope.onEditDesc = false;          
          $scope.alerts.push({type: 'success', msg: 'Property updated.'});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: "Please fill up the fields with (*) on the Description Tab."});
      }  
    }

    $scope.saveChangeMap = function (datus){
      $scope.closeAlert();
      if(datus.map){
        
        var request = $http({
          method: "post",
          url: "controller/saveChangeMapRealtyController.php",
          data: datus,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
          listRealty();
          $scope.onEditMap = false;          
          $scope.alerts.push({type: 'success', msg: 'Property updated.'});
        });
      }else{
        $scope.alerts.push({type: 'danger', msg: "Please fill up the fields with (*) on the Others Tab."});
      }  
    }

    $scope.deleteImage = function (item){
      
      if(item){
        var modalInstance = $modal.open({
          templateUrl: 'delete-image.html',
          controller: deleteImageCtrl,
          resolve: {
            item: function () {
              return item;
            }
          }
        });        
        modalInstance.result.then(
          function (result) {
            console.log('called $modalInstance.close()');
            $scope.closeAlert();
            $scope.alerts.push({type: 'danger', msg: item.file_name+' deleted.'})
          }
          );
      }
      
    }
    var deleteImageCtrl = function ($scope, $modalInstance, item) {
      console.log(item);
      $scope.item = item;
      $scope.imgID = item.imgID;
      $scope.ok = function (item){
        if(item){
          var request = $http({
            method: "post",
            url: "../../public/uploads/delete.php",
            data: item,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });
          request.success(function (data) {
            getRealty();
            $modalInstance.close('cancel');
          });
        }
      }
      $scope.close = function () {
        $modalInstance.close('cancel');
      };
    }

    /*===+ ADD image +===*/
    $scope.addImage = function (files,itemID){
      
      if(files.length > 0){
        var modalInstance = $modal.open({
          templateUrl: 'add-image-realty.html',
          controller: addImageCtrl,
          windowClass: 'app-modal-create',
          backdrop: 'static',
          resolve: {
            files: function () {
              return files;
            },
            itemID: function () {
              return itemID;
            }
          }
        });
      }
      
    }
    var addImageCtrl = function ($scope, $modalInstance, files, itemID) {
      console.log(itemID);
      console.log(files);
      $scope.itemID = itemID;  
      $scope.imgFiles = files;  

      $scope.remove = function (item) {
        var index=$scope.imgFiles.indexOf(item)
        $scope.imgFiles.splice(index,1);  
      }; 

      $scope.close = function () {
        $modalInstance.close('cancel');
      };

      /*===+ upload image +===*/
      $scope.uploadImages = function (files,itemID) {
        var file = files;
        if (file && file.length) {
          for (var i = 0; i < file.length; i++) {            
            console.log(file[i]);
            Upload.upload({
              url: '../../public/uploads/upload.php',
              data: {
                file: file[i], 
                'real_state_id': itemID,
                'folder': itemID,
              }
            }).then(function (resp) {
              console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              $scope.percent = progressPercentage;
              $scope.currentFile = evt.config.data.file.name;
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          }     
        }
      }
    }

    $scope.close = function () {
      $modalInstance.dismiss('cancel');
      listRealty();
    };
  };

  $scope.viewCategory = function (){
    var modalInstance = $modal.open({
      templateUrl: 'realty-category.html',
      controller: viewCategoryCtrl,
      backdrop: 'static'
    });
  }

  var viewCategoryCtrl = function ($scope, $modalInstance) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    var getCategory = function () {
      var requestGET = $http({
        method: "get",
        url: "controller/getCategoryController.php",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {        
        console.log(data);
        if(data){
          $scope.list = data;
        }      
      });
    }
    getCategory();

    $scope.datacat = {};
    $scope.onCatUpdate = false;

    $scope.addCategory = function (item){
      $scope.closeAlert();
      if(item.type != null){
        var request = $http({
          method: "post",
          url: "controller/addCategoryController.php",
          data: item,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        request.success(function (data) {
          getCategory();
          $scope.datacat = {};
          $scope.alerts.push({type: data.type, msg: data.result});
        });
      }
    };

    $scope.editCat = function (item){
      $scope.onCatUpdate = true;
      $scope.datacat = item;  
    }
    $scope.editCancel = function (item){
      $scope.onCatUpdate = false;
      $scope.datacat = {};  
    }
    $scope.updateCategory = function (item){ 
    $scope.closeAlert();        
      if(item.type != null){
        var request = $http({
          method: "post",
          url: "controller/editCategoryController.php",
          data: item,
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        request.success(function (data) {  
          $scope.onCatUpdate = false;        
          getCategory();
          $scope.datacat = {};
          $scope.alerts.push({type: data.type, msg: data.result});                    
        });
      }  
    };

    $scope.delete = function (item){      
      if(item){
        var modalInstance = $modal.open({
          templateUrl: 'delete.html',
          controller: deleteCtrl,
          resolve: {
            item: function () {
              return item;
            }
          }
        }); 
      }      
    }
    var deleteCtrl = function ($scope, $modalInstance, item) {
      console.log(item);
      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      $scope.item = item;
      $scope.ok = function (item){
        $scope.closeAlert();
        if(item){
          var request = $http({
            method: "post",
            url: "controller/deleteCategoryController.php",
            data: item,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });
          request.success(function (data) {
            if(data.type == 'success'){
              getCategory();
              $modalInstance.close('cancel');
              $scope.alerts.push({type: data.type, msg: data.result});
            }else{
              $scope.alerts.push({type: 'danger', msg: data.result});
            }           
          });
        }
      }
      $scope.close = function () {
        $modalInstance.close('cancel');
      };
    }

    
    $scope.close = function () {
      $modalInstance.dismiss('cancel');
      listRealty();
    };
  };  
});