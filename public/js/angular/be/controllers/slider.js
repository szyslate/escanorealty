'use strict';

myApp.controller('sliderCtrl', function($scope,$location,$http,$window,store,$modal,Upload,Config,Slug) {

  console.log(Config.baseURL);

  $scope.alerts = [];
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  var getImages = function () {
    var requestGET = $http({
      method: "get",
      url: "controller/getImagesController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.imgList = data;
      }
      
    });
  }
  getImages();

  $scope.addImage = function (files){

    if(files.length > 0){
      var modalInstance = $modal.open({
        templateUrl: 'add-image-realty.html',
        controller: addImageCtrl,
        windowClass: 'app-modal-create',
        backdrop: 'static',
        resolve: {
          files: function () {
            return files;
          }
        }
      });
    }

  }
  var addImageCtrl = function ($scope, $modalInstance, files) {
    console.log(files); 
    $scope.imgFiles = files;  

    $scope.remove = function (item) {
      var index=$scope.imgFiles.indexOf(item)
      $scope.imgFiles.splice(index,1);  
    }; 

    $scope.close = function () {
      $modalInstance.close('cancel');
    }; 
      // upload image
      
      $scope.uploadImages = function (files) {
        var file = files;
        if (file && file.length) {
          for (var i = 0; i < file.length; i++) {            
            console.log(file[i]);
            Upload.upload({
              url: '../../public/uploads/upload.php',
              data: {
                file: file[i], 
                'folder': 'slider',
              }
            }).then(function (resp) {
              getImages();
              console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            }, function (resp) {
              console.log('Error status: ' + resp.status);
            }, function (evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              $scope.percent = progressPercentage;
              $scope.currentFile = evt.config.data.file.name;
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          }     
        }
      }
    }
    $scope.deleteImage = function (item){

      if(item){
        var modalInstance = $modal.open({
          templateUrl: 'delete-image.html',
          controller: deleteImageCtrl,
          resolve: {
            item: function () {
              return item;
            }
          }
        });
        modalInstance.result.then(
          function (result) {
            console.log('called $modalInstance.close()');
            $scope.alerts.push({type: 'danger', msg: item.imgName+' deleted.'})
          }
          );
      }
      
    }
    var deleteImageCtrl = function ($scope, $modalInstance, item) {

      $scope.alerts = [];
      $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
      };
      console.log(item);
      $scope.item = item;
      $scope.imgID = item.id;
      $scope.ok = function (item){
        if(item){
          item['type'] = 'slider';
          var request = $http({
            method: "post",
            url: "../../public/uploads/delete.php",
            data: item,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });
          request.success(function (data) {
            getImages();
            $modalInstance.close('close');                               
          });
        }
      }
      $scope.close = function () {
        $modalInstance.close('cancel');
      }; 

    }


  });