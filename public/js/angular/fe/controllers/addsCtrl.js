'use strict';

myApp.controller('addsCtrl', function($scope,$location,$http,store,$window,$modal,$sce,$transition,Config,$timeout) {
  
  $timeout(function() {
    var jssor_1_options = {
      $AutoPlay: true,
      $AutoPlaySteps: 4,
      $SlideDuration: 160,
      $SlideWidth: 200,
      $SlideSpacing: 3,
      $Cols: 4,
      $ArrowNavigatorOptions: {
        $Class: $JssorArrowNavigator$,
        $Steps: 4
      },
      $BulletNavigatorOptions: {
        $Class: $JssorBulletNavigator$,
        $SpacingX: 1,
        $SpacingY: 1
      }
    };

    var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

      //responsive code begin
      //you can remove responsive code if you don't want the slider scales while window resizing
      function ScaleSlider() {
        var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
        if (refSize) {
          refSize = Math.min(refSize, 809);
          jssor_1_slider.$ScaleWidth(refSize);
        }
        else {
          window.setTimeout(ScaleSlider, 30);
        }
      }
      ScaleSlider();
      $(window).bind("load", ScaleSlider);
      $(window).bind("resize", ScaleSlider);
      $(window).bind("orientationchange", ScaleSlider);
    }, 3000);
    var listAdds = function () {
      var requestGET = $http({
        method: "get",
        url: "/app/controller/listAddsController.php",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {    
        if(data){
          $scope.addsList = data;
        }          
        console.log('adds list');
        console.log($scope.addsList);
      });
    }
    listAdds();

    /*View Section*/
    $scope.viewAdds = function (item){
      var modalInstance = $modal.open({
        templateUrl: 'view-adds.html',
        controller: viewAddsCtrl,
        // windowClass: 'app-modal-view',
        resolve: {
          item: function () {
            return item;
          }
        }
      });
    }
    var viewAddsCtrl = function ($scope, $modalInstance, item) {
      
      $scope.dataAdds = item;

      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };
    }
});