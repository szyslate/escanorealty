'use strict';

var myApp = angular.module('myModule', [
  'angular-storage',
  'ui.bootstrap',
  'ui.bootstrap.transition',
  'ngSanitize'
  ]);

myApp.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});

myApp.factory('Display', function($http){
   var Display = {
      getMainContent: function(item){
        var requestPOST = $http({
          method: "post",
          data: {
            keyword: item
          },
          url: "/app/controller/getMainPageController.php",
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        return requestPOST.success(function (data) {     
          if(data){
            return data;            
          }      
        });
      },
      listUsers: function(){
        return $http.get('/user/').then(function(response){
          return response.data;
        });
      }
   }
   return Display;
});
myApp.factory('mapsFactory', function() {
    return {
        getMaps: function(data) {
            return data
        }
    };
});

myApp.directive('carousel', [function() { 
    return { }
}]);