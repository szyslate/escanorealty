'use strict';

myApp.controller('contactCtrl', function($scope,$location,$http,store,$window,$modal,$sce,$transition,Config) {
  
  $scope.msgData = {};
  $scope.alerts = [];
  var invalidEmail = false;
  $scope.validEmail = false;
  $scope.closeAlert = function (index) {
    $scope.alerts.splice(index, 1);
  };

  function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
  };

  $scope.chkEmail = function (data){
    if( !isValidEmailAddress( data ) ) {
      $scope.validEmail = true;
      invalidEmail = true;
    }else{
      $scope.validEmail = false;
      invalidEmail = false;
    }
  }
  $scope.submit = function (datus){

    $scope.closeAlert(); 
    
    if(datus){
      if(!invalidEmail){
        if(datus.fullname && datus.contactNumber && datus.email && datus.msg){
          var request = $http({
            method: "post",
            url: "/app/controller/submitMsgController.php",
            data: datus,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          });

          request.success(function (data) {
            console.log(data);
            $scope.msgData = {};
            $scope.alerts.push({type: 'success', msg: 'Message submitted. Thank you.'});
          });
        }else{
          $scope.alerts.push({type: 'danger', msg: 'Please fill up all the fields.'});
        }
      }else{
        $scope.alerts.push({type: 'danger', msg: 'Invalid email address.'});
      }
    }else{
      $scope.alerts.push({type: 'danger', msg: 'Please fill up all the fields.'});
    }
  }

});