'use strict';

myApp.controller('mainCtrl', function($scope,$location,$http,store,$window,$sce,$interval,$timeout,Display,Config) {
  // $scope.baseURL = window.location.href;
  $scope.baseURL = $location.host();  

  var getSlider = function () {
    var requestGET = $http({
      method: "get",
      url: "/app/controller/getImagesController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.imgList = data;
        console.log($scope.imgList);
      }      
    });
  }
  getSlider();

  var getMainContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "mainpage"
      },
      url: "/app/controller/getMainPageController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.datamain = data;
      }      
    });
  }
  getMainContent();

  // $scope.search = store.get('search');
  $scope.goSearch = function (data){
    if(data.type){
      data['type']=data.type;
    }else{
      data['type']=null;
    }
    if(data.key){
      data['key']=data.key;
    }else{
      data['key']=null;
    }
    store.set('search', data);
    $window.location.href = 'property.php'; 
    console.log(store.get('search')); 
  }

  var getRealtyType = function () {
    var requestGET = $http({
      method: "get",
      url: "/app/controller/getRealtyTypeController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.realtyType = data;
      }      
    });
  }
  getRealtyType();

  var getAboutContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "about"
      },
      url: "/app/controller/getMainPageController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.about = data;
      }      
    });
  }
  getAboutContent();

  var getContactContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "contact"
      },
      url: "/app/controller/getMainPageController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.contact = data;
      }      
    });
  }
  getContactContent();  

  var getServicesContent = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "services"
      },
      url: "/app/controller/getMainPageController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {     
      if(data){
        $scope.service = data;
      }      
    });
  }
  getServicesContent();  

  var getSocial = function () {
    var requestGET = $http({
      method: "get",
      url: "/app/controller/getSocialMediaController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {
    console.log(data);   
      if(data){
        $scope.social = data;
      }      
    });
  }
  getSocial();
  
});