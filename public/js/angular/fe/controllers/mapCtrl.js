'use strict';
myApp.controller('mapCtrl', function($scope,$location,$http,store,$window,$modal,$sce,$transition,Config) {

  var getMap = function () {
    var requestGET = $http({
      method: "post",
      data: {
        keyword: "map"
      },
      url: "/app/controller/getMainPageController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {   
      if(data){
        console.log(data);
        $scope.locmap = data;
        var y = data.content.replace(/[\s]/g, '');
        var x = y.split(",")

        $scope.latLong = {
          latitude: parseFloat(x[0]),
          longitude: parseFloat(x[1])
        };
        
        goToMap(parseFloat(x[0]),parseFloat(x[1]),data.title);
      }      
    });
  }
  getMap();

  var goToMap = function (xlat,ylong,title) {
    var styleArray = [
    {
      featureType: "all",
      stylers: [
      { saturation: -80 }
      ]
    },{
      featureType: "road.arterial",
      elementType: "geometry",
      stylers: [
      { hue: "#00ffee" },
      { saturation: 50 }
      ]
    },{
      featureType: "poi.business",
      elementType: "labels",
      stylers: [
      { visibility: "off" }
      ]
    }
    ];
    var myCenter=new google.maps.LatLng(xlat, ylong);
    var mapProp = {
      center:myCenter,
      styles: styleArray,
      scrollwheel: false,
      zoom:16,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker=new google.maps.Marker({
      map: map,
      position:myCenter,
      title: title
    });
    marker.setMap(map);
  }  

});