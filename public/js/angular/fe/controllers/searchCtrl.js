'use strict';

myApp.controller('searchCtrl', function($scope,$location,$http,$timeout,store,$window,$modal,$sce,$transition,Config) {

  if(store.get('search')){

    $scope.search = store.get('search');

    var goToMap = function (xlat,ylong,title) {
      var styleArray = [
      {
        featureType: "all",
        stylers: [
        { saturation: -80 }
        ]
      },{
        featureType: "road.arterial",
        elementType: "geometry",
        stylers: [
        { hue: "#00ffee" },
        { saturation: 50 }
        ]
      },{
        featureType: "poi.business",
        elementType: "labels",
        stylers: [
        { visibility: "off" }
        ]
      }
      ];
      var myCenter=new google.maps.LatLng(xlat, ylong);
      var mapProp = {
        center:myCenter,
        styles: styleArray,
        scrollwheel: false,
        zoom:16,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      };
      var map=new google.maps.Map(document.getElementById("googleMapProp"),mapProp);

      var marker=new google.maps.Marker({
        map: map,
        position:myCenter,
        title: title
      });
      marker.setMap(map);
    } 
    
    $scope.goSearch = function (data){
      if(data.type){
        data['type']=data.type;
      }else{
        data['type']=null;
      }
      if(data.key){
        data['key']=data.key;
      }else{
        data['key']=null;
      }
      store.set('search', data);
      $window.location.href = 'property.php'; 
    }

    var getSearch = function () {
      var searchParam = {
          category: $scope.search.type,
          address: $scope.search.key,
        };
      var requestGET = $http({
        method: "post",
        data: {
          category: $scope.search.type,
          address: $scope.search.key
        },
        url: "/app/controller/getSearchController.php",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      });
      requestGET.success(function (data) {
          $scope.list = data; 
      });
    }
    getSearch();

    if($scope.list != 'false'){
      $timeout(function() {
        var pagesShown = 1;

        var pageSize = 6;

        $scope.paginationLimit = function(data) {
          return pageSize * pagesShown;
        };

        $scope.hasMoreItemsToShow = function() {
          return pagesShown < ($scope.list.length / pageSize);
        };

        $scope.showMoreItems = function() {
          pagesShown = pagesShown + 1;       
        }; 
      }, 2000);
    }
    

    /*View Section*/
    $scope.view = function (itemID){
      var modalInstance = $modal.open({
        templateUrl: 'view-realty.html',
        controller: viewDetailsCtrl,
        windowClass: 'app-modal-view',
        resolve: {
          itemID: function () {
            return itemID;
          }
        }
      });
    }
    var viewDetailsCtrl = function ($scope, $modalInstance, itemID) {
      var getRealty = function () {
        var requestGET = $http({
          method: "post",
          url: "/app/controller/getRealtyController.php",
          data: {
            real_state_id: itemID
          },
          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
        requestGET.success(function (data) {        
         
          if(data){
            var prop = data.data;
            $scope.data = prop;          
            $scope.imgFiles = data.image;
            var y = prop.map.replace(/[\s]/g, '');
            var x = y.split(",");  
            $timeout(function() {
              goToMap(parseFloat(x[0]),parseFloat(x[1]),prop.title); 
            }, 2000);
          }
        });
      }
      getRealty();

      $scope.close = function () {
        $modalInstance.dismiss('cancel');
      };
    }
  }

});