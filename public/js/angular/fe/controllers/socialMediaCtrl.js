'use strict';

myApp.controller('socialMediaCtrl', function($scope,$location,$http,store,$window,$modal,$sce,$transition,Config) {
  var getContent = function () {
    var requestGET = $http({
      method: "get",
      url: "/app/controller/getSocialMediaController.php",
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    });
    requestGET.success(function (data) {   
      if(data){
        $scope.social = data;
      }      
    });
  }
  getContent();
});