'use strict';

myApp.filter('decodeUtf8', function() {

  return function(input) {
    if(input){
      return decodeURIComponent(escape(input));
    }
    
  }

});