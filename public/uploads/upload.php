<?php
include '../../app/config/config.php';
$target_dir = $_POST['folder'];
if($target_dir == 'adds'){
    $temp = explode(".", $_FILES["file"]["name"]);
    $newfilename = $_POST['adds_id'] . '.' . end($temp);
    $target_file = $target_dir .'/'. basename($newfilename);
}else{
   $target_file = $target_dir .'/'. basename($_FILES["file"]["name"]); 
}

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size 1000000 = 1MB
if ($_FILES["file"]["size"] > 2000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    
    if(!is_dir($target_dir)) {
        mkdir($target_dir);
    }

    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        
        if($target_dir == 'slider'){
            $sql = "INSERT INTO slider SET";
            $sql .= " 
            imgName = '".$_FILES["file"]["name"]."',
            title = 'image title',
            description = 'image description',
            status = '1',
            date_created = '".date('Y-m-d H:i:s')."',
            date_updated = '".date('Y-m-d H:i:s')."'
            ";
        }else if($target_dir == 'adds'){
            $sql = "UPDATE adds SET";
            $sql .= " image = '".$newfilename."' WHERE adds_id LIKE '".$_POST['adds_id']."'";
        }else{
            $sql = "INSERT INTO real_state_images SET";
            $sql .= " 
            real_state_id = '".$_POST['real_state_id']."', 
            file_name = '".$_FILES["file"]["name"]."',
            date_created = '".date('Y-m-d H:i:s')."',
            date_updated = '".date('Y-m-d H:i:s')."'
            ";
        }

        

        if ($conn->query($sql) === TRUE) {
            $data = array(
              'result' => 'success',
              );
        } else {
            $data = array(
              'result' => 'error',
              'error_msg' => $conn->error
              );
        }  
        $conn->close();
        echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>