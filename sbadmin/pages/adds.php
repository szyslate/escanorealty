<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-adds.html">
  <div ng-include="'tpls/add-adds.html'"></div>
</script>
<script type="text/ng-template" id="edit-adds.html">
  <div ng-include="'tpls/edit-adds.html'"></div>
</script>
<script type="text/ng-template" id="delete-adds.html">
  <div ng-include="'tpls/delete-adds.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="addsPageCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Adds</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <button type="button" ng-click="createAdds()" class="btn btn-primary btn-sm">
              <i class="fa fa-plus"></i> Create Adds
            </button>
          </div>
        </div>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            Adds List
          </div>
          <div class="panel-body">
            <div class="col-md-4 col-sm-6" ng-repeat="x in list">
              <div class="adds-list" style="background-image: url('../../public/uploads/adds/{[{x.image}]}');">
                <label class="realty-title" ng-click="editAdds(x)">
                  <span ng-bind="x.title"></span>
                </label> 
                <label class="realty-delete" ng-click="deleteAdds(x)">
                  <a href=""><i class="fa fa-trash-o"></i></a>
                </label>           
              </div>              
            </div>
          </div>
        </div>
      </div>                
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

