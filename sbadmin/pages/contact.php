<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-realty.html">
  <div ng-include="'tpls/add-realty.html'"></div>
</script>
<script type="text/ng-template" id="edit-realty.html">
  <div ng-include="'tpls/edit-realty.html'"></div>
</script>
<script type="text/ng-template" id="add-image-realty.html">
  <div ng-include="'tpls/add-image-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete-image.html">
  <div ng-include="'tpls/delete-image.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="contactPageCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Contact</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
          <span ng-bind="alert.msg"></span>
        </alert>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            Contact Details
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label>Title <i class="req">*</i></label>
              <input class="form-control" name="" id="" ng-model="data.title">
            </div>
            <div class="form-group">
              <label>Content <i class="req">*</i></label>
              <text-angular ng-model="data.content"></text-angular>
            </div>  
          </div>

        <div class="panel-footer">       
          <button type="button" ng-click="saveChange(data)" class="btn btn-success btn-sm">Save Change</button>          
        </div>
        </div>
      </div>  
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            Map
          </div>
          <div class="panel-body">
            <!-- <div class="form-group">
              <label>Paste embeded Google Map <i class="req">*</i></label>
              <textarea class="form-control" ng-model="map.content" ng-change="pasteMap(map.content)"></textarea>
            </div>  --> 
            <div class="form-group">
              <label>Complete Address <i class="req">*</i></label>
              <input class="form-control" placeholder="Map Address" ng-model="locmap.title" ng-change="pasteMap(locmap.content)">              
            </div>  
            <div class="form-group">
              <label>Paste Latitude and Longitude <i class="req">*</i></label>
              <input class="form-control" placeholder="Latitude, Longitude" ng-model="locmap.content" ng-change="pasteMap(locmap.content)">              
            </div>  
            <div class="form-group">
              <ui-gmap-google-map center='map.center' zoom='map.zoom' draggable="true" options="options">
                <ui-gmap-marker coords="marker.coords" options="marker.options" events="marker.events" idkey="marker.id">
                  <ui-gmap-windows show="show">
                    <div ng-non-bindable>{[{locmap.title}]}</div>
                  </ui-gmap-windows>
                </ui-gmap-marker>
              </ui-gmap-google-map>
            </div>  

            <div class="form-group" ng-show="locMap">
            <iframe src="{[{locMap}]}" width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
            </div>  
          </div>

        <div class="panel-footer">       
          <button type="button" ng-click="saveChange(locmap)" class="btn btn-success btn-sm">Save Change</button>          
        </div>
        </div>
      </div>                
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

