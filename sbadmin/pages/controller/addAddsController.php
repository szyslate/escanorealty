<?php
include '../../../app/config/config.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
  
  
  $adds_id = uniqid();
  
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "INSERT INTO adds SET";
  $sql .= " 
  adds_id = '".$adds_id."', 
  title = '".$request->title."',
  description = '".$request->description."',
  status = '".$request->status."',
  date_created = '".date('Y-m-d H:i:s')."',
  date_updated = '".date('Y-m-d H:i:s')."'
  ";

  if ($conn->query($sql) === TRUE) {
    $data = array(
      'result' => 'success',
      'adds_id' => $adds_id
      );
  } else {
    $data = array(
      'result' => 'error',
      'error_msg' => $conn->error
      );
  }  
  // $data = $row; 
  $conn->close();
  // Set session variables
  
  echo json_encode($data);
  
}


?>
