<?php
include '../../../app/config/config.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
  
  
  $real_state_id = uniqid();
  
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "INSERT INTO real_estate SET";
  $sql .= " 
  real_state_id = '".$real_state_id."', 
  title = '".$request->title."',
  description = '".$request->description."',
  address = '".$request->address."',
  orig_price = '".$request->orig_price."',
  discounted_price = '".$request->discounted_price."',
  category = '".$request->category."',
  status = '".$request->status."',
  date_created = '".date('Y-m-d H:i:s')."',
  date_updated = '".date('Y-m-d H:i:s')."'
  ";

  if ($conn->query($sql) === TRUE) {
    $data = array(
      'result' => 'success',
      'real_state_id' => $real_state_id
      );
  } else {
    $data = array(
      'result' => 'error',
      'error_msg' => $conn->error
      );
  }  
  // $data = $row; 
  $conn->close();
  // Set session variables
  
  echo json_encode($data);
  
}


?>
