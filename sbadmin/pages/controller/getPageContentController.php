<?php
include '../../../app/config/config.php';
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

function utf8_string_array_encode(&$array){
    $func = function(&$value,&$key){
        if(is_string($value)){
            $value = utf8_encode($value);
        } 
        if(is_string($key)){
            $key = utf8_encode($key);
        }
        if(is_array($value)){
            utf8_string_array_encode($value);
        }
    };
    array_walk($array,$func);
    return $array;
}

$keyword = $request->keyword;
$query = "SELECT * FROM pages WHERE keyword LIKE '".$keyword."'";
$result = $conn->query($query);

$row = ($result) ? $result->fetch_assoc() : 'error';
$data = utf8_string_array_encode($row);
// var_dump($data);

$conn->close();  
echo json_encode($data);
?>
