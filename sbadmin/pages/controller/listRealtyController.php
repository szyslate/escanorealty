<?php
include '../../../app/config/config.php';

  
  
  $query = "SELECT * FROM real_estate";
  $query .= " ORDER BY id DESC";

  $result = $conn->query($query);

  if($result){
    
    while ($row = $result->fetch_object()) {   
      $query2 = "SELECT * FROM real_state_images WHERE real_state_id='".$row->real_state_id."'";
      $query2 .= " ORDER BY id ASC LIMIT 1";
      $images = $conn->query($query2);
      if($images){
        while ($img_row = $images->fetch_object()) { 
          $img_array = $img_row->file_name;
            
        } 
      }
      $data[] = array(
        'real_state_id' => $row->real_state_id,
        'title' => $row->title,
        'description' => $row->description,
        'address' => $row->address,
        'orig_price' => $row->orig_price,
        'discounted_price' => $row->discounted_price,
        'category' => $row->category,
        'status' => $row->status,
        'date_created' => $row->date_created,
        'date_updated' => $row->date_updated,
        'image' => $img_array
        );  

       
      $results_array = $data;   
    } 
  } 
  header('Content-Type: application/json');
  $conn->close();
  
  echo json_encode($results_array);
?>
