<?php
include '../../../app/config/config.php';

  
  
  $query = "SELECT * FROM messages";
  $query .= " ORDER BY date_created DESC";

  $result = $conn->query($query);

  if($result){
    
    while ($row = $result->fetch_object()) {
      $results_array[] = $row;   
    } 
  } 
  header('Content-Type: application/json');
  $conn->close();
  
  echo json_encode($results_array);
?>
