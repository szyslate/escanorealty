<?php
include '../../../app/config/config.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
  
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  $content = $conn->real_escape_string($request->content);

  $sql = 'UPDATE pages SET';
  $sql .= ' title = "'.$request->title.'",';
  $sql .= ' content = "'.$content.'"';
  $sql .= ' WHERE keyword LIKE "'.$request->keyword.'"';

  if ($conn->query($sql) === TRUE) {
    $data = array(
      'result' => 'Update success.',
      'type' => 'success',
      'keyword' => $request->keyword
      );
  } else {
    $data = array(
      'result' => 'Update error. Please try again.',
      'type' => 'danger',
      'error_msg' => $conn->error
      );
  }  
  // $data = $row; 
  $conn->close();
  // Set session variables
  // var_dump($data);
  echo json_encode($data);
  
}


?>
