<?php
include '../../../app/config/config.php';

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if($request){
  
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  } 

  if($request->status== 0){
    $msg="Property set as Draft.";
  }else if($request->status== 1){
    $msg="Property set as Posted.";
  }else if($request->status== 2){
    $msg="Property set as Sold.";
  }

  $sql = "UPDATE real_estate SET";
  $sql .= " status = '".$request->status."'";
  $sql .= " WHERE real_state_id LIKE '".$request->real_state_id."'";

  if ($conn->query($sql) === TRUE) {
    $data = array(
      'result' => $msg,
      'real_state_id' => $request->real_state_id
      );
  } else {
    $data = array(
      'result' => 'error',
      'error_msg' => $conn->error
      );
  }  
  // $data = $row; 
  $conn->close();
  // Set session variables
  
  echo json_encode($data);
  
}


?>
