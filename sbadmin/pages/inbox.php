<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="view-msg.html">
  <div ng-include="'tpls/view-msg.html'"></div>
</script>
<script type="text/ng-template" id="delete-msg.html">
  <div ng-include="'tpls/delete-msg.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="inboxCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Inbox</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      </div>              
    </div>
    <div class="row">
      <div class="col-lg-12">
        
        <div class="panel panel-default">
          <div class="panel-heading">
            Messages
          </div>
          <!-- /.panel-heading -->
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact #</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                  <tr ng-repeat="y in msgData">
                    <td></td>
                    <td>
                      <a class="mr unread" href ng-click="viewMsg(y)" ng-bind="y.fullname"></a>
                    </td>
                    <td ng-bind="y.email"></td>
                    <td ng-bind="y.contactNumber"></td>
                    <td ng-bind="y. date_created"></td>
                  </tr>                  
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
            </div>
          </div>
        </div>
      </div>                
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

