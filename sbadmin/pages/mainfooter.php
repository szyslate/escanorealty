<!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- AngularJS -->
    <script src="../../public/js/angular/angular.js"></script>
    <script src="../../public/js/angular/be/controllers/app.js"></script>
    <script src="../../public/js/angular/directives.js"></script>
    <script src="../../public/js/angular/filters.js"></script>
    <script src="../../public/js/angular/be/controllers/config.js"></script>
    <script src="../../public/js/angular/be/controllers/realty.js"></script>
    <script src="../../public/js/angular/be/controllers/slider.js"></script>
    <script src="../../public/js/angular/be/controllers/mainpage.js"></script>
    <script src="../../public/js/angular/be/controllers/contact.js"></script>
    <script src="../../public/js/angular/be/controllers/about.js"></script>
    <script src="../../public/js/angular/be/controllers/services.js"></script>
    <script src="../../public/js/angular/be/controllers/socialmedia.js"></script>
    <script src="../../public/js/angular/be/controllers/adds.js"></script>
    <script src="../../public/js/angular/be/controllers/inbox.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
    <script src="//rawgit.com/angular-ui/angular-google-maps/2.1.5/dist/angular-google-maps.min.js"></script>
    <!-- // <script src="//maps.googleapis.com/maps/api/js"></script> -->
    <script
    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCbdYfbrRhOiLgmIec6d1I7rHjZ6eoY7XM">
    </script>

    <script src="../../public/vendor/angular-storage-master/dist/angular-storage.min.js"></script>
    <!-- // <script src="../../public/vendor/angular-sanitize.min.js"></script> -->
    <script src="../../public/vendor/ui-bootstrap-tpls.min.js"></script>
    <script src="../../public/vendor/ng-file-upload.min.js"></script>

    <!-- slugifier -->
    <script src="../../public/vendor/angular-slugify.js"></script>

    <!-- textAngular -->
    <script src="../../public/vendor/textAngular-master/dist/textAngular-rangy.min.js"></script>
    <script src="../../public/vendor/textAngular-master/dist/textAngular-sanitize.min.js"></script>
    <script src="../../public/vendor/textAngular-master/dist/textAngular.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
   <!--  // <script src="../bower_components/raphael/raphael-min.js"></script>
    // <script src="../bower_components/morrisjs/morris.min.js"></script>
    // <script src="../js/morris-data.js"></script> -->

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>