<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-realty.html">
  <div ng-include="'tpls/add-realty.html'"></div>
</script>
<script type="text/ng-template" id="realty-category.html">
  <div ng-include="'tpls/realty-category.html'"></div>
</script>
<script type="text/ng-template" id="edit-realty.html">
  <div ng-include="'tpls/edit-realty.html'"></div>
</script>
<script type="text/ng-template" id="add-image-realty.html">
  <div ng-include="'tpls/add-image-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete-image.html">
  <div ng-include="'tpls/delete-image.html'"></div>
</script>
<script type="text/ng-template" id="delete-realty.html">
  <div ng-include="'tpls/delete-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete.html">
  <div ng-include="'tpls/delete.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="addPropertyCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Realty</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <button type="button" ng-click="addProp()" class="btn btn-primary btn-sm">
              <i class="fa fa-plus"></i> Property
            </button>
            <button type="button" ng-click="viewCategory()" class="btn btn-primary btn-sm">
              Category
            </button>
          </div>
        </div>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            Real States
          </div>
          <div class="panel-body">
            <div class="col-md-4 col-sm-6" ng-repeat="x in list">
              <div class="realty-list" style="background-image: url('../../public/uploads/{[{x.real_state_id}]}/{[{x.image}]}');">
                <label class="realty-title" ng-click="editProp(x.real_state_id)">
                  <span ng-bind="x.title"></span>
                </label> 
                <label class="realty-delete" ng-click="deleteRealty(x)">
                  <a href=""><i class="fa fa-trash-o"></i></a>
                </label>           
              </div>              
            </div>
          </div>
        </div>
      </div>                
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

