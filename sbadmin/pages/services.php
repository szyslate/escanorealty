<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-realty.html">
  <div ng-include="'tpls/add-realty.html'"></div>
</script>
<script type="text/ng-template" id="edit-realty.html">
  <div ng-include="'tpls/edit-realty.html'"></div>
</script>
<script type="text/ng-template" id="add-image-realty.html">
  <div ng-include="'tpls/add-image-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete-image.html">
  <div ng-include="'tpls/delete-image.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="servicesPageCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Services</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
          <span ng-bind="alert.msg"></span>
        </alert>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            About Details
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label>Title <i class="req">*</i></label>
              <input class="form-control" name="" id="" ng-model="data.title">
            </div>
            <div class="form-group">
              <label>Content <i class="req">*</i></label>
              <text-angular ng-model="data.content"></text-angular>
            </div>  
          </div>

        <div class="panel-footer">       
          <button type="button" ng-click="saveChange(data)" class="btn btn-success btn-sm">Save Change</button>          
        </div>
        </div>
      </div> 
                    
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

