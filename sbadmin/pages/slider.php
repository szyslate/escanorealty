<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-realty.html">
  <div ng-include="'tpls/add-realty.html'"></div>
</script>
<script type="text/ng-template" id="edit-realty.html">
  <div ng-include="'tpls/edit-realty.html'"></div>
</script>
<script type="text/ng-template" id="add-image-realty.html">
  <div ng-include="'tpls/add-image-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete-image.html">
  <div ng-include="'tpls/delete-image.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="sliderCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Slider</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">           
            <button type="button" 
            class="btn btn-primary btn-sm" 
            ngf-select="addImage(files)" 
            ng-model="files" 
            ngf-multiple="true"
            ngf-pattern="'image/*'"
            ngf-accept="'image/*'">
              <i class="fa fa-plus"></i> Images
            </button>
          </div>
        </div>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            Images
          </div>
          <div class="panel-body">
            <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">{[{ alert.msg }]}</alert>
            <div class="col-md-4 col-sm-6" ng-repeat="imgList in imgList">
              <div class="realty-list" style="background-image: url('../../public/uploads/slider/{[{imgList.imgName}]}');">
                  <label class="realty-title">
                    <!-- <span class="small-f" ng-bind="imgList.imgName"></span> -->
                    <a href="" class="r-s" ng-click="deleteImage(imgList)">
                      <i class="fa fa-times-circle-o"></i>
                    </a>
                  </label>           
              </div>              
            </div>
          </div>
        </div>
      </div>                
    </div>
  </div>
</div>

<?php include 'mainfooter.php';?>

