<?php
include '../../app/config/config.php';
include 'mainhead.php';
?>
<script type="text/ng-template" id="add-realty.html">
  <div ng-include="'tpls/add-realty.html'"></div>
</script>
<script type="text/ng-template" id="edit-realty.html">
  <div ng-include="'tpls/edit-realty.html'"></div>
</script>
<script type="text/ng-template" id="add-image-realty.html">
  <div ng-include="'tpls/add-image-realty.html'"></div>
</script>
<script type="text/ng-template" id="delete-image.html">
  <div ng-include="'tpls/delete-image.html'"></div>
</script>

<div id="wrapper">

  <?php include 'navigation.php';?>

  <div id="page-wrapper" ng-controller="socialMediaPageCtrl">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Social Media</h1>
      </div>                
    </div>
    <div class="row">
      <div class="col-lg-12">
        <alert ng-repeat="alert in alerts" type="{[{alert.type }]}" close="closeAlert($index)">
          <span ng-bind="alert.msg"></span>
        </alert>
      </div>
      <div class="col-lg-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            List of Social Media
          </div>
          <div class="panel-body">
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-facebook-square"></i></span>
              <input ng-model="data.facebook" type="text" class="form-control" placeholder="Link here">
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
              <input ng-model="data.twitter" type="text" class="form-control" placeholder="Link here">
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
              <input ng-model="data.linkedin" type="text" class="form-control" placeholder="Link here">
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
              <input ng-model="data.instagram" type="text" class="form-control" placeholder="Link here">
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
              <input ng-model="data.google" type="text" class="form-control" placeholder="Link here">
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-youtube"></i></span>
              <input ng-model="data.youtube" type="text" class="form-control" placeholder="Link here">
            </div>
          </div>

        <div class="panel-footer">       
          <button type="button" ng-click="saveChange(data)" class="btn btn-success btn-sm">Save Change</button>          
        </div>
        </div>
      </div> 
                    
    </div>


  </div>


</div>

<?php include 'mainfooter.php';?>

